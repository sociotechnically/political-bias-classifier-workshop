# Political Bias Classifier Workshop

This is an evolving collection of materials relating to workshops I've done on how to build a classifier.

## Students, see 'Community Data Science Class Demo' Folder
## R Users, see 'R Studio' Folder
## Python Users, see 'Python/Jupyter' Folder for more scripts
