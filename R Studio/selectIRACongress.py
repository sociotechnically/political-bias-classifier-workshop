#!/usr/bin/env python3

###############################################################################
##
## The purpose of this script is to filter through congressional speech and limit
## it to IRA-related speech
##
## 1 - Process args
##
## 2 - Read through each JSON file in turn
##
## 3 - Report results
##
###############################################################################
##
## {"header": {...}, "content": [{"kind": "speech", "speaker": "...", "text": "" }]}
## 
## this json file is a list of items, and the bits we want are 'kind, speaker, text, ....' inside {}
##
##

import json
from os import listdir
from os.path import isfile, join
import glob
import re


outfile = './iraFull.tsv'
## prod file
#searchString = "output/2020/" + "*/json/*.json"
## test file
#searchString = "output/2021/" + "*/json/*.json"
fileList = glob.glob('output/2021/*/json/*.json') + glob.glob('output/2022/*/json/*.json')
print(fileList)
outFH = open(outfile, 'w')
outFH.write(f"speechdate\tspeakername\tspeech\n")
ira_re = re.compile(r'(inflation reduction|build back better|5376)', re.IGNORECASE)


for filename in fileList:
    with open(filename) as fh:
        jData = json.load(fh)
        byUtterance = jData["content"]
        for u in byUtterance:
            if (u["kind"] != "speech"):
                continue
            iraMatches = ira_re.search(u["text"])
            if (iraMatches): #write the speech to our record
                speechdate = filename.split('/')[-1]
                speechdate = speechdate[:-5]
                speakername = u["speaker"]
#                partyAff = partyList[speakername]
                speech = u["text"]
                speech = speech.replace('\n', '')#trim the \ns from the text
                outFH.write(f"{speechdate}\t{speakername}\t{speech}\n")

